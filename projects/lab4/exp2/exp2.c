//LABORAT�RIO 04 - MICROCONTROLADORES
//ALUNOS: Lucas Mendes e Gabriel Anisio


 // conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

//Vari�veis auxiliares
int start = 3, convertido = 0, soma_conv = 0,  i = 0, j = 0;
float convertidoTemp = 0, temp_conv = 0;
float VinMedia = 0;
char tensaoSTRMedia[5];
char tempSTR[5];
unsigned char m;

//************** Fun��o OUTCHR_UART2(char c) ********************
void OUTCHR_UART2(unsigned char c){
     Delay_ms(5);
     while (U2STAbits.UTXBF); // espera enquanto o buffer de Tx est� cheio.
     U2TXREG = c; // escreve caractere.
}

void OUTSTR_UART2(unsigned char * s){
     i=0;
     while(s[i]) // la�o at� *s == �\0�, fim da string
     OUTCHR_UART2(s[i++]); // envia o caractere e pronto para o pr�ximo
}

//Interrup��o para inser��o de caractere no terminal
void Interrupt_U2RX() iv IVT_ADDR_U2RXINTERRUPT{
     IFS1bits.U2RXIF = 0; // zera o flag
     m = U2RXREG;
     OUTCHR_UART2(m);

     switch(m){    //faz a compara��o de cada uma das poss�veis enntradas. O valor de start determinada o que ser� realizado em seguida
           case 'A':
           start = 1;    //habilita a aquisi��o da tens�o por meio de potenci�metro
           break;

           case 'T':
           start = 2;  // (CRIATIVIDADE: -> habilita a aquisi��o da temperatura por meio de sensor
           break;

           case 'P':   //Para o processo de aquisi��o
           start = 0;
           break;

           default:
           OUTSTR_UART2("\r\n");
           OUTSTR_UART2("Comando inv�lido!!! \r\n");
     }
}

//Inicializa��o manual do conversor A/D
void init_ADC_man(){

     ADCON1 = 0; // controle de sequencia de convers�o manual
     ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
     ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��o quando � convers�o manual)
     ADCON3=0x0007;//Tad= 4 * Tcy = 4*62,5 ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V)   12Tad = 250ns * 12 = 3us
     ADCON1bits.ADON = 1; // liga o ADC
}

//Faz a amostra do sinal
int ler_ADC_man( int canal){
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    Delay_us(163); //tempo de amostragem  (1/6000) = 166us   (166us - 12Tad) = (166us - 3us) = 163us
    ADCON1bits.SAMP = 0; // termina a amostragem
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // retorna o resultado da convers�o.
}

//Inicializacao da Porta UART2
void INIT_UART2 (unsigned char valor_baud)
{
        U2BRG = valor_baud;
        /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
        U2MODE = 0x0000; //Ver tabela para saber as outras configura��es
        U2STA = 0x0000;
        IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
        IFS1bits.U2TXIF = 0; //Zerar o flag de interrup��o de Tx.
        IEC1bits.U2TXIE = 0; //Habilita interrup��o de Tx.
        IFS1bits.U2RXIF = 0; //Zerar o flag de interrup��o de Rx.
        IEC1bits.U2RXIE = 1; //Habilita interrup��o de Rx.
        U2MODEbits.USIDL = 1;
        U2MODEbits.UARTEN = 1; //E liga a UART
        U2STAbits.UTXEN = 1;
}

// ------------------------ PROGRAMA PRINCIPAL--------------------------------------------//
void main(){
     ADPCFG = 0xFE7F; // Pino RB8 como entrada anal�gica (0 = anal�gica, 1=digital)
     TRISB = 0;
     TRISBbits.TRISB8 = 1; // Porta B8 como entrada
     TRISBbits.TRISB7 = 1; // Porta B7 como entrada

     //Inicializando o LCD
     Lcd_Init();
     Lcd_Cmd(_LCD_CLEAR);
     Lcd_Cmd(_LCD_CURSOR_OFF);

     init_ADC_man();  //inicializa conversor
     INIT_UART2(51);  //incializa comunicacao UART -> velocidade de 19200

     OUTSTR_UART2("--------------------- MENU ---------------------\r\n");
     OUTSTR_UART2("Digite 'A' para comecar a aquisicao \r\n");
     OUTSTR_UART2("Digite 'T' para comecar a aquisicao de Temperatura \r\n");
     OUTSTR_UART2("Digite 'P' para parar a aquisicao \r\n");


     while(1){
          //Modulo de Aquisi��o de Tens�o
          if(start==1){
              for(j = 0; j < 16; j++){          //loop para 16 convers�es
                   convertido = ler_ADC_man(8); //Ler o valor convertido na entrada anal�gicado ADC
                   soma_conv = convertido + soma_conv; //soma os valores convertidos
              }
              VinMedia = (soma_conv/16) * 0.004887;    //calculo da media e condicionamento do sinal
              soma_conv = 0;

              FloatToStr(VinMedia, tensaoSTRMedia);         //convertendo de float para string
              //Mostra no PC
              OUTSTR_UART2("\r\n");
              OUTSTR_UART2("A m�dia �:\r\n");
              OUTSTR_UART2(tensaoSTRMedia);
              OUTSTR_UART2("\r\n");

              //Mostra LCD
              Lcd_Cmd(_LCD_CLEAR);
              Lcd_Out(1,1,"TENSAO MEDIA: ");
              Lcd_Out(2,1,tensaoSTRMedia);
              Delay_ms(1000);
          }  //Fim do Modulo de Aquisi��o de Tens�o

          //Modulo de Aquisi��o de Temperatura
          if (start==2){
              convertidoTemp = ler_ADC_man(7); //Ler o valor convertido na entrada anal�gicado ADC
              temp_conv = (convertidoTemp*4.2)/1023;

              FloatToStr(temp_conv*45, tempSTR);         //convertendo de float para string
              //Mostra no PC
              OUTSTR_UART2("A Temperatura �: ");
              OUTSTR_UART2(tempSTR);
              OUTSTR_UART2("�C\r\n");

              //Mostra LCD
              Lcd_Cmd(_LCD_CLEAR);
              Lcd_Out(1,1,"TEMPERATURA: ");
              Lcd_Out(2,1,tempSTR); // Mostra valor da tensao no LCD
              Lcd_Out(2,10,"C");
              Delay_ms(1000);
           }//Fim do m�dulo de aquisi��o de Temperatura

           //M�dulo de Parada
           if(start == 0){
              Lcd_Cmd(_LCD_CLEAR);
              OUTSTR_UART2("\r\n");
              OUTSTR_UART2("A conversao parou, pressionar 'A' ou 'T' para come�ar de novo.\r\n");
              start = 3;  //garante que nenhum m�dulo ser� acionado
           } //Fim do m�dulo de parada
       } //Fim do while

   } //Fim da main