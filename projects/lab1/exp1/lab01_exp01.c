/*

EXPERIÊNCIA 1

Usando as teclas SW0 e SW1 e os displays de 7 segmentos será implementado um sistema 
que deve iniciar mostrando os caracteres “----” e a medida que o usuário esteja 
digitando a senha os “-” devem ser substituídos pelos respectivos caracteres. Você 
procura o caractere com a tecla SW0 e a tecla SW1 confirma esse caractere fazendo 
com que o buzzer apite, ou seja, depois de ter escolhido o caractere este será 
armazenado e depois é que o buzzer apita. Os números serão deslocados de direita 
para esquerda, como em uma calculadora. Este sistema tem um código interno “secreto” 
que são os caracteres UFS145, que terá que ser descoberto pelo usuário de tal forma que:

a) Quando se digitam os caracteres UFS145 (só nessa ordem U,F,..,1, 4,....) o código 
secreto, devem ser mostrados os caracteres digitados se deslocando de direita para 
esquerda e depois do último digito mostrar a palavra “dSPIC30F4011” deslocando-se 
para a esquerda tendo 2 espaços vazios antes de cada repetição. Sinal de que está 
correto o número digitado.

b) Se digitarem os caracteres diferentes de UFS145 (a verificação dos caracteres 
deve ser depois de entrarem todos eles), se devem mostrar os caracteres digitados 
se deslocando de direita para esquerda e depois do último digito mostrar a palavra 
"Erro" piscando a intervalos de 0,5 seg aproximadamente.

c) Se digitar três vezes os caracteres errados deverá ser ativado o buzzer durante 
5 segundos e novamente fica habilitado para entrar o número.

d) Se deve criar alguma técnica de parada para os itens a) e b) de tal forma que 
possamos executar algum dos outros itens.

Deverão ser usadas funções e é obrigatório fazer os FLUXOGRAMAS do programa. 
Os caracteres serão reconhecidos depois de soltar a tecla. FAZER O PROGRAMA EM 
C QUE POSSA EXECUTAR O ENUNCIADO E UMA BOA CRIATIVIDADE (ou seja, acrescentar a 
criatividade ao programa principal).

*/

#include "kitports.h"	/* Define macros para alguns registradores*/
#include "strcmp.h"		/* Função strcmp da biblioteca String do C */

#define TRUE 1
#define FALSE 0

unsigned int j, temp, errorCount, pressedSW0, pressedSW1, reset;
int i;

char password[] = "------",
	content[] = "----",
	initScreenText[] = "----",
	storedPassword[] = "UFS145",
	sucessScreenText[] = "dsPIC30F4011  ",
	errorScreenText[] = "Erro",
	alphaNumericChars[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'I', 'L', 
							'n', 'O', 'P', 'q', 'r', 'S', 'U', 'y', 'Z',
							'0', '1', '2', '3', '4', '5', '6', '7', '8','9'
						  };

/**
 * Função que retorna os correstpondentes dos caracteres em hexadecimal para o 
 * display de 7 segmentos
 * @param  letter caractere desejado
 * @return        hexadecimal correspondente
 */
unsigned int mask(char letter){
        switch(letter){
                case 'A' : return 0x77;
                case 'a' : return 0x77;
                case 'B' : return 0x7C;
                case 'b' : return 0x7C;
                case 'C' : return 0x39;
                case 'c' : return 0x58;
                case 'd' : return 0x5E;
                case 'D' : return 0x5E;
                case 'E' : return 0x79;
                case 'e' : return 0x79;
                case 'F' : return 0x71;
                case 'f' : return 0x71;
                case 'g' : return 0x6F;
                case 'G' : return 0x6F;
                case 'h' : return 0x74;
                case 'H' : return 0x76;
                case 'i' : return 0x30;
                case 'I' : return 0x30;
                case 'l' : return 0x38;
                case 'L' : return 0x38;
                case 'n' : return 0x54;
                case 'N' : return 0x54;
                case 'o' : return 0x5C;
                case 'O' : return 0x3F;
                case 'P' : return 0x73;
                case 'p' : return 0x73;
                case 'q' : return 0x67;
                case 'Q' : return 0x67;
                case 'r' : return 0x50;
                case 'R' : return 0x50;
                case 'S' : return 0x6D;
                case 's' : return 0x6D;
                case 't' : return 0x78;
                case 'T' : return 0x31;
                case 'u' : return 0x1C;
                case 'U' : return 0x3E;
                case 'y' : return 0x6E;
                case 'Y' : return 0x6E;
                case 'Z' : return 0x5B;
                case 'z' : return 0x5B;
                case '.' : return 0x80;
                case '-' : return 0x40;
                case '_' : return 0x08;
                case '0' : return 0x3F;
                case '1' : return 0x06;
                case '2' : return 0x5B;
                case '3' : return 0x4F;
                case '4' : return 0x66;
                case '5' : return 0x6D;
                case '6' : return 0x7D;
                case '7' : return 0x07;
                case '8' : return 0x7F;
                case '9' : return 0x6F;
                default : return 0x00;
        }
}

/**
 * Desliga os displays de 7 segmentos
 */
void turnDisplaysOff(){
    DISP0 = 0;
    DISP1 = 0;
    DISP2 = 0;
    DISP3 = 0;    
}

/**
 * Exibe mensagem (text) nos 3 primeiros displays, separando o último caractere (letter)
 * quarto display (esquerda para a direita) para o efeito "como na calculadora"
 * @param text   mensagem a ser exibida nos 3 primeiros displays
 * @param letter caractere a ser exibido no quarto display
 */
void showOnDisplay(char *text, char letter){

        int k, temp = 10;
        
        while(temp > 0){
                DISP3 = 1;
                LATB = mask(content[3]);
                Delay_ms(3);
                DISP3 = 0;

                DISP2 = 1;
                LATB = mask(content[2]);
                Delay_ms(3);
                DISP2 = 0;

                DISP1 = 1;
                LATB = mask(content[1]);
                Delay_ms(3);
                DISP1 = 0;

                DISP0 = 1;
                LATB = mask(content[0]);
                Delay_ms(3);
                DISP0 = 0;

                /* LEDs vermelhos */
                LATFbits.LATF0 = 1;
                LATB = 0x03;
                Delay_ms(3);
                LATFbits.LATF0 = 0;

                temp--;
        }       
}

/**
 * Exibe mensagem nos displays de 7 segmentos se deslocando da direita para a esquerda, também
 * controla os LEDs verdes
 * @param content mensagem a ser exibida
 * @param size    tamanho da mensagem
 * @param leds    ativar LEDs
 */
void showOnDisplayRolling(char *content, unsigned int size, unsigned int leds){

        int i, temp;

        size /= sizeof(*content);

        for(i = 0; i < size - 2; i++)
        {
                temp = 10;
                while(temp > 0){
                        DISP3 = 1;
                        LATB = mask(content[i]);
                        Delay_ms(3);
                        DISP3 = 0;

                        DISP2 = 1;
                        LATB = mask(content[i + 1]);
                        Delay_ms(3);
                        DISP2 = 0;

                        DISP1 = 1;
                        LATB = mask(content[i + 2]);
                        Delay_ms(3);
                        DISP1 = 0;

                        DISP0 = 1;
                        LATB = mask(content[i + 3]);
                        Delay_ms(3);
                        DISP0 = 0;

                        /* LEDs verdes */
                        if(leds == 1){
                            LATFbits.LATF0 = 1;
                            LATB = 0xC0;
                            Delay_ms(3);
                            LATFbits.LATF0 = 0;
                        }

                        temp--;
                }
        }        
}

/**
 * Exibe a mensagem estaticamente nos displays
 * @param content mensagem a ser exibida
 */
void showOnDisplayAtomic(char *content){

        int temp = 10;
        while(temp > 0){
                DISP3 = 1;
                LATB = mask(content[0]);
                Delay_ms(3);
                DISP3 = 0;

                DISP2 = 1;
                LATB = mask(content[1]);
                Delay_ms(3);
                DISP2 = 0;

                DISP1 = 1;
                LATB = mask(content[2]);
                Delay_ms(3);
                DISP1 = 0;

                DISP0 = 1;
                LATB = mask(content[3]);
                Delay_ms(3);
                DISP0 = 0;

                temp--;
        }
}

/**
 * Exibe a mensagem piscando nos displays
 * @param content mensagem a ser exibida
 */
void showOnDisplayAtomicBlink(char *content){

        int temp = 5;
        while(temp > 0){

                showOnDisplayAtomic(content);                
                temp--;
        }
}

/**
 * Efeito de ativação circular dos segmentos dos quatro displays de 7
 * segementos
 */
void animaDisplay(){
    temp = 1;
    LATFbits.LATF0 = 1;
    while(temp > 0){

        /* segmento superior */
        DISP3 = 1;
        LATB = 0x01;
        Delay_ms(15);
        DISP3 = 0;

        DISP2 = 1;
        LATB = 0x01;
        Delay_ms(15);
        DISP2 = 0;

        DISP1 = 1;
        LATB = 0x01;
        Delay_ms(15);
        DISP1 = 0;

        DISP0 = 1;
        LATB = 0x01;
        Delay_ms(15);
        DISP0 = 0;

        /* segmento lateral direito */
        DISP0 = 1;
        LATB = 0x02;
        Delay_ms(15);
        DISP0 = 0;

        DISP0 = 1;
        LATB = 0x04;
        Delay_ms(15);
        DISP0 = 0;

        /* segmento inferior */
        DISP0 = 1;
        LATB = 0x08;
        Delay_ms(15);
        DISP0 = 0;       

        DISP1 = 1;
        LATB = 0x08;
        Delay_ms(15);
        DISP1 = 0;

        DISP2 = 1;
        LATB = 0x08;
        Delay_ms(15);
        DISP2 = 0;

        DISP3 = 1;
        LATB = 0x08;
        Delay_ms(15);
        DISP3 = 0;

        /* segmento lateral esquerdo */        
        DISP3 = 1;
        LATB = 0x10;
        Delay_ms(15);
        DISP3 = 0;

        DISP3 = 1;
        LATB = 0x20;
        Delay_ms(15);
        DISP3 = 0;


        temp--;
    }
    LATFbits.LATF0 = 0;
}

/**
 * Efeito de ativação circular dos segmentos dos quatro displays de 7
 * segmentos individualmente
 */
void animaDisplayIndividual()
{
	temp = 4;
    while(temp > 0){

    		/* LEDs amarelos */
            LATFbits.LATF0 = 1;
            LATB = 0x04;
            Delay_ms(15);
            LATFbits.LATF0 = 0;

            DISP3 = 1;
            LATB = 0x01;
            Delay_ms(50);
            DISP3 = 0;

            DISP3 = 1;
            LATB = 0x02;
            Delay_ms(50);
            DISP3 = 0;            

            DISP3 = 1;
            LATB = 0x04;
            Delay_ms(50);
            DISP3 = 0;

            DISP3 = 1;
            LATB = 0x08;
            Delay_ms(50);
            DISP3 = 0;

            DISP3 = 1;
            LATB = 0x10;
            Delay_ms(50);
            DISP3 = 0;                        

            DISP3 = 1;
            LATB = 0x20;
            Delay_ms(50);
            DISP3 = 0;

            /* LEDs amarelos */
            LATFbits.LATF0 = 1;
            LATB = 0x08;
            Delay_ms(15);
            LATFbits.LATF0 = 0;

            DISP2 = 1;
            LATB = 0x01;
            Delay_ms(50);
            DISP2 = 0;

            DISP2 = 1;
            LATB = 0x02;
            Delay_ms(50);
            DISP2 = 0;            

            DISP2 = 1;
            LATB = 0x04;
            Delay_ms(50);
            DISP2 = 0;

            DISP2 = 1;
            LATB = 0x08;
            Delay_ms(50);
            DISP2 = 0;

            DISP2 = 1;
            LATB = 0x10;
            Delay_ms(50);
            DISP2 = 0;                        

            DISP2 = 1;
            LATB = 0x20;
            Delay_ms(50);
            DISP2 = 0;

            /* LEDs amarelos */
            LATFbits.LATF0 = 1;
            LATB = 0x10;
            Delay_ms(15);
            LATFbits.LATF0 = 0;            

            DISP1 = 1;
            LATB = 0x01;
            Delay_ms(50);
            DISP1 = 0;

            DISP1 = 1;
            LATB = 0x02;
            Delay_ms(50);
            DISP1 = 0;            

            DISP1 = 1;
            LATB = 0x04;
            Delay_ms(50);
            DISP1 = 0;

            DISP1 = 1;
            LATB = 0x08;
            Delay_ms(50);
            DISP1 = 0;

            DISP1 = 1;
            LATB = 0x10;
            Delay_ms(50);
            DISP1 = 0;                        

            DISP1 = 1;
            LATB = 0x20;
            Delay_ms(50);
            DISP1 = 0;

            /* LEDs amarelos */
            LATFbits.LATF0 = 1;
            LATB = 0x20;
            Delay_ms(15);
            LATFbits.LATF0 = 0;            

            DISP0 = 1;
            LATB = 0x01;
            Delay_ms(50);
            DISP0 = 0;

            DISP0 = 1;
            LATB = 0x02;
            Delay_ms(50);
            DISP0 = 0;            

            DISP0 = 1;
            LATB = 0x04;
            Delay_ms(50);
            DISP0 = 0;

            DISP0 = 1;
            LATB = 0x08;
            Delay_ms(50);
            DISP0 = 0;

            DISP0 = 1;
            LATB = 0x10;
            Delay_ms(50);
            DISP0 = 0;                        

            DISP0 = 1;
            LATB = 0x20;
            Delay_ms(50);
            DISP0 = 0;
        
        temp--;
    }   	
}

/**
 * Ativa o buzzer por 5 segundos e exibe animação nos displays de 7 segmentos
 */
void playAlertSound(){
    
    BZ1 = 1;
    
    animaDisplayIndividual();

    Delay_ms(200);
    BZ1 = 0;
}

/**
 * Ativa o buzzer com sinal de atenção
 */
void playWarningSound(){
        
    BZ1 = 1;
    Delay_ms(30);
    BZ1 = 0;
}

/**
 * Ativa o buzzer com sinal de "sistema pronto"
 */
void playReadySound(){
	    BZ1 = 1;
        Delay_ms(30);
        BZ1 = 0;
        Delay_ms(100);
        BZ1 = 1;
        Delay_ms(30);
        BZ1 = 0;
}

/**
 * Restaura a variável password para seu estado original e limpa o conteudo dos
 * displays de 7 segmentos para o padrão
 * @param password variável a ser restaurada
 */
void resetPassword(char *password)
{
	int l;
	for(l = 0; password[l] != '\0'; l++)
		password[l] = '-';
	for(l = 0; l < 4; l++)
		content[l] = '-';
}

/**
 * Rotina de inserção de senha
 * @param change flag de alteração da senha armazenada no sistema
 */
void insertPassword(unsigned int change){
     playReadySound();
     i = 0;
     while(i < 28 && j < 6){

        if(!SW0 == 1){
            pressedSW0 = TRUE;
        }

        if(!SW1 == 1) {
            pressedSW1 = TRUE;
        }

        /* Exibe a senha inserida com o efeito "calculadora" */
        showOnDisplay(password, alphaNumericChars[i]);

        if (pressedSW1){

        	/* Operações do efeito "calculadora" */
            content[3] = content[2];
            content[2] = content[1];

            /* Checagem da flag de alteração de senha armazenada no sistema*/
            if(change == 1){ 
                storedPassword[j] = alphaNumericChars[i - 1];
                content[1] = storedPassword[j];
            } else {
                password[j] = alphaNumericChars[i - 1];
                content[1] = password[j];
            }

            j++;
            i = 0;
            content[0] = '-';

            playWarningSound();	/* Ativação breve do Buzzer para a confirmação do caractere */
            pressedSW1 = FALSE;
        }

        if (pressedSW0){ 
            content[0] = alphaNumericChars[i];
            i++;
            pressedSW0 = FALSE;
        }

    }
}


int main (void)
{
	/* Porta B no modo digital */
    ADPCFG = 0xFFFF;

    /* Porta B como saída */
    TRISB = 0;
    
    LATB = 0;

    /* Displays de 7 segmentos */
    TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD2 = 0;
    TRISCbits.TRISC13 = 0;
    TRISCbits.TRISC14 = 0;

    /* Fileira de Leds */
    TRISFbits.TRISF0 = 0;

    /* Buzzer */
    TRISDbits.TRISD3 = 0;

    i = 0;
    j = 0;
    errorCount = 0;
    reset = FALSE;
    
    while(1)
    {
            if(j < 6){	/* enquanto todos os caracteres da senha não forem inseridos */
            		insertPassword(reset);
                    
                    /* caso a senha armazenada tenha sido alterada, volta para o estado de bloqueio */
                    if(reset == TRUE){
                        showOnDisplayRolling(" Sucesso  ", 8, TRUE);
                        animaDisplay();
                        reset = FALSE;
                        j = 0; i = 0;
                        resetPassword(&password);
                        resetPassword(&content);                            
                    }

            } else { /* depois da senha inserida e armazenada */
                    if(strcmp(password, storedPassword) == 0){ /* caso a senha esteja correta */
                        showOnDisplayRolling(password, sizeof(password), TRUE);	/* Exibe a senha inserida */
                        showOnDisplayRolling(sucessScreenText, sizeof(sucessScreenText), TRUE); /* Exibe a mensagem "dsPIC30F4011" */

                        errorCount = 0;
                        
                        /* Pressionar o botão SW0 neste estado resultará no bloqueio do sistema */
                        if(!SW0 == 1){ 
                            showOnDisplayRolling(" Bloqueado", 10, TRUE); /* Exibe a mensagem "bloqueado"*/
                            animaDisplay();	/* Executa animação nos displays de 7 segmentos */

                            j = 0; i = 0;
                            resetPassword(&password);
                            resetPassword(&content);                            
                        }

                        /* Pressionar SW1 neste estado acionará a flag de troca da senha armazenada no sistema */
                        if(!SW1 == 1){
                            showOnDisplayRolling(" resetar senha", 13, TRUE);
                            j = 0; i = 0; reset = TRUE;
                            resetPassword(&password);
                            resetPassword(&content);      
                        }
                    }
                    else { /* caso a senha esteja errada */
                                    showOnDisplayRolling(password, sizeof(password) - 1, FALSE); /* Exibe a senha inserida*/
                                    showOnDisplayAtomicBlink(errorScreenText); /* Exibe a mensagem de "Erro" */
                                    
                                    
                                    /* Ativa LEDs vermelhos quando as mensagem saírem da tela*/
                                    LATFbits.LATF0 = 1;
                                    LATB = 0x03;
                                    Delay_ms(5);
                                    LATFbits.LATF0 = 0;
                                    
                                    /* Caso alguma das teclas do painel seja pressionada uma nova tentativa será dada (caso ainda reste) */
                                    if(!SW0 == 1 || !SW1 == 1){
                                        errorCount++;
                                        
                                        /* Notifica o usuário das tentativas restantes e do bloqueio quando estas se esgotam */
                                        switch(errorCount){
                                        	case 1: showOnDisplayRolling(" 2 restantes", 12, FALSE); 
                                                    animaDisplay();
                                                    break;
                                        	case 2: showOnDisplayRolling(" 1 restante", 11, FALSE); 
                                                    animaDisplay(); 
                                                    break;
                                        	case 3: showOnDisplayRolling(" Bloqueado", 11, FALSE); 
                                                    break;
                                        }

                                        j = 0; i = 0;
                                        resetPassword(&password);
                                        resetPassword(&content);
                                    }

                                    if(errorCount > 2){ /* ativa o buzzer por 5s na terceira tentativa errada */
                                        
                                        /* Trava botões do painel*/
                                        TRISDbits.TRISD1 = 0;
                                        TRISFbits.TRISF6 = 0;
                                        
                                        playAlertSound();
                                        errorCount = 0;
                                        animaDisplay();

                                        /* Destrava botões do painel */
                                        TRISDbits.TRISD1 = 1;
                                        TRISFbits.TRISF6 = 1;                                        
                                    }
                    }
            }
    }
}