# Experiência 2

Neste sistema serão utilizadas duas teclas para simular um sensor de ultrassom que mede a distância de algum objeto. Com uma tecla pressionada se simula um objeto se afastando do sensor e com a outra tecla pressionada um objeto se aproximando do sensor. Só poderá ser pressionada uma tecla por vez. Nos displays de 7 segmentos serão mostrados os valores das distâncias em centímetros. Um sensor de ultrassom tem um alcance de 2 cm até 4 m aproximadamente. O sistema funcionará de acordo com a seguinte tabela:

| Distância do objeto ao sensor |    Buzzer apita de acordo    |       Relés      |
|:-----------------------------:|:----------------------------:|:----------------:|
|          Acima de 2 m         | Fica desligado completamente | Relés desligados |
|       Entre 1,8 m e 2 m       |          A cada 3 s          |    Liga Relé 1   |
|      Entre 1,2 m e 1,8 m      |          A cada 2 s          |    Liga Relé 2   |
|      Entre 80 cm e 1,2 m      |          A cada 1 s          |    Liga Relé 3   |
|      Entre 50 cm e 80 cm      |         A cada 0,75 s        |    Liga Relé 4   |
|      Entre 20 cm e 50 cm      |         A cada 0,5 s         |    Liga Relé 5   |
|        Menos que 20 cm        |   Fica ligado completamente  |    Liga Relé 6   |