/*

Experi�ncia 4

Utilizando duas teclas (uma para escolher o valor de PWM e outra para confirmar) mudar a 
velocidade dos motores do carrinho. O valor do PWM dever� aparecer nos displays de 7 
segmentos (a multiplexa��o destes deve ser feito com interrup��es).

O funcionamento do  sistema ser� o seguinte:

* O sistema come�a parado (os displays de 7 segmentos desligados e o carrinho parado);

* Depois de pressionar e soltar (ativar) a tecla de interrup��o INT0:

        * Ficam habilitadas as duas teclas para mudar o valor do PWM;

        * Os valores escolhidos devem ser mostrados no display de 7 segmentos (lembrem que 
          os n�meros devem ser deslocados nos displays quando se pressionam as teclas).

* Depois de confirmar o valor do PWM o carrinho deve ser ativado de acordo com o valor 
  definido (valor que deve ser mostrado no display de 7 segmentos). Quando se confirma 
  um novo valor do PWM a velocidade do carrinho tamb�m mudar�, se o valor do PWM n�o 
  foi confirmado a velocidade do carrinho continua com o valor do PWM anterior.

A frequ�ncia de opera��o do carrinho ser� arbitrariamente determinada. Os valores de 
PWM ser�o de 0 a 100% em passos de 1%.

Pensem em alguma forma de usar o kit de rel�s com as l�mpadas neste exerc�cio, ser� parte 
do funcionamento do circuito. Depois voc�s podem usar o kit de rel�s com as l�mpadas para 
a criatividade se quiserem.

Determinar qual � o valor m�nimo de duty cycle para o qual o motor come�a a girar.

*/

unsigned char enablePWM = 0;
int displayCounter = 0;
unsigned int nPWM = 0, dutyCycle, counterDutyCycle, 
centenas, dezenas, unidades, 
numbers[10] = {0x3F,0x06,0x05B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};

/**
 * Interrup��o externa 2 utilizada para lidar com as intera��es 
 * do bot�o de incremento do duty cycle
 */
void Ext2Int() iv IVT_ADDR_INT2INTERRUPT ics ICS_AUTO
{
        Delay_ms(100);  // Delay para atenuar o efeito do bounce
        
        if(nPWM < 100)
            nPWM++;
        else
            nPWM = 0;

        IFS1bits.INT2IF = 0;    // Limpa flag de interrup��o
}

/**
 * Interrup��o externa 0 utilizada para lidar com as intera��es 
 * do bot�o de ativa��o/confirma��o do sistema
 */
void Ext0Int() iv IVT_ADDR_INT0INTERRUPT ics ICS_AUTO
{
    Delay_ms(100); // Delay para atenuar o efeito do bounce

    enablePWM = 1;  // Vari�vel de ativa��o do sistema
    dutyCycle = nPWM;   // Altera��o do duty cycle corrente pelo 
                        // novo valor escolhido
    
    IFS0bits.INT0IF = 0;    // Limpa flag de interrup��o
}

/**
 * Interrup��o do Timer 1 utilizada para multiplexar os displays de 
 * 7 segmentos e realizar a separa��o das casas decimais do valor do PWM
 */
void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{

    /* Desliga os displays de 7 segmentos */
    LATDbits.LATD2 = 0;
    LATCbits.LATC14 = 0;
    LATCbits.LATC13 = 0;

    /* Separa as  casas decimais do n�mero do PWM */
    
    /* Esse m�todo utiliza um vetor com os caracteres dos displays
     * indexados com a posi��o correspondente num�rica. � uma outra
     * forma de obter o mesmo resultado da fun��o mask(), que � 
     * desencorajada dentro da rotina de tratamento da interrup��o.
     */
    
    centenas = nPWM/100;
    dezenas = (nPWM-centenas*100)/10;
    unidades = nPWM-centenas*100-dezenas*10;

    if(enablePWM == 1){           // Checagem da vari�vel de ativa��o
        switch(displayCounter)  // Modifica o comportamento de acordo com o display ativo
        {
            case 0:
                    if(nPWM == 100){                // Caso a centena seja atinjida
                            LATDbits.LATD2 = 1;     // Liga display das centenas
                            LATB = numbers[centenas];   // Exibe valor da centena
                        }
                        else 
                            LATDbits.LATD2 = 0;     // Desliga display das centenas
                        break;

            case 1:
                    if(nPWM >= 10){                 // Caso o valor tenha casa decimal
                            LATCbits.LATC14 = 1;    // Liga o display da casa decimal
                            LATB = numbers[dezenas];    // Exibe o valor ca dasa decimal
                        }
                        else 
                            LATCbits.LATC14 = 0;    // Desliga o display da casa decimal
                        break;

            case 2:
                    LATCbits.LATC13 = 1;        // Display das unidades sempre ligado
                    LATB = numbers[unidades];   // Exibe unidades
                    displayCounter = -1;        // Zera contador dos displays
                    break;
        }
    }

    displayCounter++;       // Incrementa contador dos displays a cada nova chamada da interrup��o
    IFS0bits.T1IF = 0;      // Zera flag de interrup��o
}

/**
 * Interrup��o do Timer 2 utilizado para gerar a onda do PWM
 */
void Timer2Int() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
    IFS0bits.T2IF = 0;      // Zera flag de interrup��o

    if(counterDutyCycle > 100 - dutyCycle){ // Duty cyle positivo
        LATDbits.LATD0 = 1;
        LATDbits.LATD3 = 1;
    } else {                        // Duty cycle negativo
        LATDbits.LATD0 = 0;
        LATDbits.LATD3 = 0;
    }


    if(counterDutyCycle > 99)       // Granularidade do valor de PWM (1%)
        counterDutyCycle = 1;
    else
        counterDutyCycle++;
}


/**
 * Fluxo principal do programa
 */
void main ()
{
    ADPCFG = 0xFFFF; // PORTB como digital
    TRISB = 0; // PORTB como sa�da
    TRISF = 0; // PORTF como sa�da

    /* Configura sa�das da habilita��o dos displays de 7 segmentos */
    TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD2 = 0;
    TRISCbits.TRISC13 = 0;
    TRISCbits.TRISC14 = 0;

    /* Configura sa�da do PWM */
    TRISDbits.TRISD0 = 0;

    /* Configura sa�da do buzzer */
    TRISDbits.TRISD3 = 0;

    /* Configura sa�das do kit de rel�s */
    TRISEbits.TRISE0 = 0;
    TRISEbits.TRISE1 = 0;
    TRISEbits.TRISE2 = 0;
    TRISEbits.TRISE3 = 0;
    TRISEbits.TRISE4 = 0;
    TRISEbits.TRISE5 = 0;

    /* Configura interrup��es externas dos bot�es (0 e 2) */
    TRISEbits.TRISE8 = 1;
    TRISDbits.TRISD1 = 1;
    IFS0 = 0; // Limpa flags de interrup��o
    IFS1 = 0;
    IEC0bits.INT0IE = 1; // Habilitamos INT0
    IEC1bits.INT2IE = 1; // Habilitamos INT2
    INTCON2bits.INT0EP = 0; //Borda positiva
    INTCON2bits.INT2EP = 0; //Borda positiva


    /* Configura timer 1 para a multiplexa��o dos displays */
    IEC0bits.T1IE = 1; // Habilita T1
    PR1 = 1000;        // T1 de 62.5 us
    T1CON = 0x8000; // Ativa T1

    /* Configura timer 2 para a gera��o do sinal PWM */
    IEC0bits.T2IE = 1; // Habilita T2
    PR2 = 1600;        // T2 de 100 us
    T2CON = 0x8000; // Ativa T2

    while(1){
        if(enablePWM == 1) { // Vari�vel de ativa��o
            
            /* Motor esquerdo avante */
            LATFbits.LATF0 = 0;
            LATFbits.LATF1 = 1;

            /* Motor direito avante */
            LATFbits.LATF4 = 1;
            LATFbits.LATF5 = 0;

            /* Comportamento do kit de rel�s */
            if((dutyCycle >= 0) && (dutyCycle < 15)){
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 1;
                LATEbits.LATE2 = 1;
                LATEbits.LATE3 = 1;
                LATEbits.LATE4 = 1;
                LATEbits.LATE5 = 1;
            } else if((dutyCycle >= 15) && (dutyCycle < 30)){
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 0;
                LATEbits.LATE2 = 1;
                LATEbits.LATE3 = 1;
                LATEbits.LATE4 = 1;
                LATEbits.LATE5 = 1;                                
            } else if((dutyCycle >= 30) && (dutyCycle < 50)){
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 0;
                LATEbits.LATE2 = 0;
                LATEbits.LATE3 = 1;
                LATEbits.LATE4 = 1;
                LATEbits.LATE5 = 1;                                                                
            } else if((dutyCycle >= 50) && (dutyCycle < 75)){
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 0;
                LATEbits.LATE2 = 0;
                LATEbits.LATE3 = 0;
                LATEbits.LATE4 = 1;
                LATEbits.LATE5 = 1;                                                                                                
            } else if((dutyCycle >= 75) && (dutyCycle < 90)){
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 0;
                LATEbits.LATE2 = 0;
                LATEbits.LATE3 = 0;
                LATEbits.LATE4 = 0;
                LATEbits.LATE5 = 1;
            } else {
                LATEbits.LATE0 = 0;
                LATEbits.LATE1 = 0;
                LATEbits.LATE2 = 0;
                LATEbits.LATE3 = 0;
                LATEbits.LATE4 = 0;
                LATEbits.LATE5 = 0;
            } 

            Delay_ms(1000);
        }
    }

}