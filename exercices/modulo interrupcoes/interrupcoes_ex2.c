/*

Projetar um sistema que mostre nos displays de 7 segmentos a palavra HOLA se
deslocando para a esquerda ou direita. Usando interrupção externa fazer com que
a palavra HOLA se desloque para a esquerda e depois quando acontecer outra 
interrupção externa a palavra HOLA se desloca para a direita e assim sucessivamente.

*/

/* Leds */
#define LED0 LATBbits.LATB0
#define LED1 LATBbits.LATB1
#define LED2 LATBbits.LATB2
#define LED3 LATBbits.LATB3
#define LED4 LATBbits.LATB4
#define LED5 LATBbits.LATB5
#define LED6 LATBbits.LATB6
#define LED7 LATBbits.LATB7

/* Displays 7 segmentos */
#define DISP0 LATCbits.LATC13
#define DISP1 LATCbits.LATC14
#define DISP2 LATDbits.LATD2
#define DISP3 LATDbits.LATD0

/* Push buttons */
#define SW0 PORTDbits.RD1
#define SW1 PORTFbits.RF6

/* Buzzer */
#define BZ1 LATDbits.LATD3

unsigned int i, direction;

void INT01Int() iv IVT_ADDR_INT0INTERRUPT ics ICS_AUTO {
	direction = 0;
	Delay_ms(20);
	IFS0bits.INT0IF = 0;
}

void INT02Int() iv IVT_ADDR_INT2INTERRUPT ics ICS_AUTO {
	direction = 1;
	Delay_ms(20);
	IFS1bits.INT2IF = 0;
}

unsigned int mask(char letter){
	switch(letter){
		case 'A' : return 0x77;
		case 'a' : return 0x77;
		case 'B' : return 0x7C;
		case 'b' : return 0x7C;
		case 'C' : return 0x39;
		case 'c' : return 0x58;
		case 'd' : return 0x5E;
		case 'D' : return 0x5E;
		case 'E' : return 0x79;
		case 'e' : return 0x79;
		case 'F' : return 0x71;
		case 'f' : return 0x71;
		case 'g' : return 0x6F;
		case 'G' : return 0x6F;
		case 'h' : return 0x74;
		case 'H' : return 0x76;
		case 'i' : return 0x30;
		case 'l' : return 0x38;
		case 'L' : return 0x38;
		case 'n' : return 0x54;
		case 'N' : return 0x54;
		case 'o' : return 0x5C;
		case 'O' : return 0x3F;
		case 'P' : return 0x73;
		case 'p' : return 0x73;
		case 'q' : return 0x67;
		case 'Q' : return 0x67;
		case 'r' : return 0x50;
		case 'R' : return 0x50;
		case 'S' : return 0x6D;
		case 's' : return 0x6D;
		case 't' : return 0x50;
		case 'T' : return 0x31;
		case 'u' : return 0x1C;
		case 'U' : return 0x3E;
		case 'y' : return 0x6E;
		case 'Y' : return 0x6E;
		case 'Z' : return 0x5B;
		case 'z' : return 0x5B;
		case '.' : return 0x80;
		default : return 0x00;
	}
}

void showOnDisplayRolling(char *content, unsigned int size){

	int i, temp;

	size /= sizeof(*content);

	if(direction == 0){
		for(i = 0; i < size - 2; i++)
		{
			temp = 10;
			while(temp > 0){
				DISP3 = 1;
				LATB = mask(content[i]);
				Delay_ms(3);
				DISP3 = 0;

				DISP2 = 1;
				LATB = mask(content[i + 1]);
				Delay_ms(3);
				DISP2 = 0;

				DISP1 = 1;
				LATB = mask(content[i + 2]);
				Delay_ms(3);
				DISP1 = 0;

				DISP0 = 1;
				LATB = mask(content[i + 3]);
				Delay_ms(3);
				DISP0 = 0;
				
				temp--;
			}
		}
	} else {
		for(i = 0; i < size - 2; i++)
		{
			temp = 10;
			while(temp > 0){
				DISP3 = 1;
				LATB = mask(content[i]);
				Delay_ms(3);
				DISP3 = 0;

				DISP2 = 1;
				LATB = mask(content[i - 1]);
				Delay_ms(3);
				DISP2 = 0;

				DISP1 = 1;
				LATB = mask(content[i - 2]);
				Delay_ms(3);
				DISP1 = 0;

				DISP0 = 1;
				LATB = mask(content[i - 3]);
				Delay_ms(3);
				DISP0 = 0;
				
				temp--;
			}
		}		
	}        
}


void main() {
	ADPCFG = 0xFFFF;
	TRISB = 0; //a PORTB como saída

	TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD2 = 0;
    TRISCbits.TRISC13 = 0;
    TRISCbits.TRISC14 = 0;

	TRISE = 0x0100; //entrada INT0=RE8
	IFS0 = 0;
	IFS2 = 0;
	//desabilita o flag desta interrupção
	IEC0bits.INT0IE = 1; //Habilitamos INT0
	INTCON2bits.INT0EP = 0; //Borda positiva

	IEC1bits.INT2IE = 1; //Habilitamos INT0
	INTCON2bits.INT2EP = 0; //Borda positiva

	while(1){
		showOnDisplayRolling("    HOLA    ", 12);
	}
}