/*
Exerc�cio 7: Usando as teclas ligadas a RF0, RF1,
RF2 e RF3 ligar os leds correspondentes quando se
pressiona alguma tecla (ex: pressionando a tecla
ligada a RF0 liga LED0 e assim por diante). Quando
se liga o sistema os leds estar�o desligados.
*/

//************* Programa Principal *************************
int main (void)
{
	ADPCFG = 0xFFFF;
	TRISB = 0x00; //a PORTB como sa�da
	TRISFbits.TRISF0 = 0x1;
	TRISFbits.TRISF1 = 0x1;
        TRISFbits.TRISF2 = 0x1;
        TRISFbits.TRISF3 = 0x1;

	LATB = 0xFF;

	// Loop infinito
	while( 1 )
	{
		if(PORTFbits.RF0 == 0x1)
			LATBbits.LATB0 = 0x1;
		if(PORTFbits.RF0 == 0x0)
            LATBbits.LATB0 = 0x0;

		if(PORTFbits.RF1 == 0x1)
            LATBbits.LATB1 = 0x1;
		if(PORTFbits.RF1 == 0x0)
            LATBbits.LATB1 = 0x0;

		if(PORTFbits.RF2 == 0x1)
            LATBbits.LATB2 = 0x1;
		if(PORTFbits.RF2 == 0x0)
            LATBbits.LATB2 = 0x0;

		if(PORTFbits.RF3 == 0x1)
            LATBbits.LATB3 = 0x1;
		if(PORTFbits.RF3 == 0x0)
            LATBbits.LATB3 = 0x0;
	}
}