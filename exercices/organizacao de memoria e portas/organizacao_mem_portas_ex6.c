/*
Exerc�cio 6: Com a mesma tecla do exemplo 5 fazer
o seguinte:

	* Se RF6 = 1 ent�o ligar e desligar os leds sequencialmente
		de 0 a 7 em intervalos de 500 ms

	* Se RF6 = 0 ent�o ligar e desligar os leds sequencialmente
		de 7 a 0 em intervalos de 500 ms
*/


//********* Vari�veis Globais *******************************
unsigned char num[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
int j;


//************* Programa Principal *************************
int main (void)
{
	ADPCFG = 0xFFFF;
	TRISB = 0x00; //a PORTB como sa�da
	TRISFbits.TRISF6 = 0x1; //botao 1 como entrada
	LATB = 0x00;

	// Loop infinito
	while( 1 )
	{
		if(PORTFbits.RF6 == 0x1)
		{
			for(j = 0 ; j < 8 ; j++){ // Blink LED 0, 1, 2, 3, 4, 5, 6, 7
				LATB = num[j] ; // Porta Sa�da Leds
				Delay_ms(500);												}
			}
		else {
			// for(j = 7 ; j < 0 ; j--)
			j = 7;
			while(j > 0){ // Blink LED 7, 6, 5, 4, 3, 2, 1, 0
				LATB = num[j] ; // Porta Sa�da Leds
				j -= 1;
				Delay_ms(500);
			}
		}
	}
}