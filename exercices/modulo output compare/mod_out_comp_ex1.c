/*

1. O modulo output compare pode ser usado para gerar uma variedade de formatos
de modulação, portanto fazer o programa para cada uma das modulações
mostradas na figura (vide arquivo 'MODULO OUTPUT COMPARE.pdf'). Fazer um exemplo 
com o seguinte código binário para cada modulação 0101010101011, o valor de 
TE será de 100 us e utilizamos os mesmos valores de cristal e PLL dos 
exercícios passados.

i) Pulse Width Modulation
ii) Pulse Position Modulation
iii) Manchester
iv) Variable Pulse Width Modulation

A modulação por largura de pulso (Pulse Width modulation – PWM) pode ser usado
para uma variedade de tarefas desde controle da intensidade de leds até controle de
velocidade de motores elétricos. Todas as aplicações a continuação se baseiam no
principio básico do incremento do sinal PWM, ou seja, se incrementa o duty cycle, a
média da tensão e a potência são fornecidas pelo incremento do PWM. O duty cycle se
incrementa também linearmente, como se mostra na figura (vide arquivo 'MODULO OUTPUT 
COMPARE.pdf'), notamos que a tensão RMS e máxima são funções do duty cycle (DC)

						
						VRMS = DC x VMAX


Decidindo sobre a frequência PWM : em geral, a frequência PWM é dependente da
aplicação, ainda que duas regras gerais devem ser levadas em consideração. Elas são:


	* Quando a frequência se incrementa, se deve prestar atenção às perdas por
	chaveamento

	* As capacitâncias e indutâncias da carga tendem a limitar a resposta em
	frequência de um circuito

Em aplicações de baixa potência, é uma boa ideia usar a frequência mínima possível
para acompanhar uma tarefa e limitar as perdas por chaveamento. Em circuitos onde a
capacitância e/ou indutância são um fator, a frequência PWM será escolhida baseada
em uma análise do circuito.
Uma importante consideração quando se escolhe a frequência PWM para a aplicação
de um controle de motor é a resposta do motor a mudanças no duty cycle da PWM.
Um motor deverá ter respostas velozes a mudanças no duty cycle em frequências
maiores.
PWM é também usado em aplicações com LEDS. O “Flicker” pode ser notável com
frequências abaixo de 50 Hz. Portanto, é uma boa regra que os LEDs trabalhando com
PWM usem frequências de 100 Hz ou maiores.

*/