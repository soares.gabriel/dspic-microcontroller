/*
Exemplo 1: Exibir contagem de 0 a 9 num dos displays de 7 segmentos
*/

unsigned int mask(unsigned int num){
        switch(num){
                case 0 : return 0x3F;
                case 1 : return 0x06;
                case 2 : return 0x5B;
                case 3 : return 0x4F;
                case 4 : return 0x66;
                case 5 : return 0x6D;
                case 6 : return 0x7D;
                case 7 : return 0x07;
                case 8 : return 0x7F;
                case 9 : return 0x6F;
        }
}

unsigned int j;


int main(){
        ADPCFG = 0xFFFF;

        TRISB = 0x00; //a PORTB como sa�da
        LATB = 0x00;

        TRISDbits.TRISD0 = 0;
        TRISDbits.TRISD2 = 0;
        TRISCbits.TRISC13 = 0;
        TRISCbits.TRISC14 = 0;

        // Loop infinito
        while(1)
        {
                  LATDbits.LATD0 = 1;
                  LATDbits.LATD2 = 1;
                  LATCbits.LATC13 = 1;
                  LATCbits.LATC14 = 1;
                  LATB = 0x06;
        }
}