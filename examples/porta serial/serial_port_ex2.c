/*Exercício 2. Escrever uma função (saída de um caractere) chamada 
OUTCHR_UART1 para transmitir um código ASCII (7 bits) de um caractere. 
Utilizar a função INIT_UART para um baud rate de 9600. Seguir configuração 
da UART do item 1 e usar o PLL8.*/

//void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void)
void interrupt() iv IVT_ADDR_U1TXInterrupt ics ICS_AUTO
{
	IFS0bits.U1TXIF = 0;
}

//*********** Função INIT_UART1(valor_baud) **********************
/*Funciona com uma frequência de 8 Mhz de clock, e ativando as interrupções de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) – 1] onde INT = inteiro. */
void INIT_UART1 (valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configurações
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade média, não é urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrupção de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrupção de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrupção de Rx.
	IEC0bits.U1RXIE = 0; //Habilita interrupção de Rx.
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

//************** Função OUTCHR_UART1(char c) ********************
void OUTCHR_UART1(unsigned char c)
{
	while ( U1STAbits.UTXBF); // espera enquanto o buffer de Tx está cheio.
	U1TXREG = c; // escreve caractere.
}

/************* Programa Principal *************************/
void main (void)
{
	INIT_UART1(103); //Inicializa com 9600 bps
	OUTCHR_UART1(0x30); //Escrevendo o número 0.
	while (1);
}