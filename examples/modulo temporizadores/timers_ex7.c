/*
Exemplo: Ligar e desligar um led na porta B ***aproximadamente uma vez a cada dois segundos***. O exemplo
usa o timer2 e 3 concatenados sendo que o clock do timer é 256 vezes mais lento que o clock no
dispositivo dsPIC. A cada 100 000 clocks do timer a rotina de interrupção é chamada e o led é desligado ou
ligado.
*/

/*A SOLUÇÃO ABAIXO ESTÁ CERTA ????*/

void __attribute__ ((interrupt, no_auto_psv)) _T3Interrupt(void)
{
	IFS0bits.T3IF = 0;
	LATBbits.LATB0 = ~LATBbits.LATB0; //Complementamos o bit.
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0;
	//a PORTB como saída
	IFS0=0;
	//Flag de interrupção do timer3
	LATB=0;
	IEC0 = IEC0 | 0x0080; // bit 7 do registrador IEC0 habilita a interrupção do timer3 (IEC0bits.T3IE=1)
	PR2 = 34464; //A interrupção do período é 100 000 clocks
	PR3 = 0x0001; //Total PR3/2 = 1*65536 + 34464 ==> PR3/2=PR3*65536 + PR2
	T2CON=0x8038; //ativamos o timer2/3 e o Prescaler fica em 256 (clock interno é dividido por 256).
	while(1); //laço infinito
}