//Criar uma função para ter atrasos na ordem dos milisegundos.

//*************Funçao Delay em ms ***********************
Delay_ms(unsigned int tempo)
{
	while (tempo != 0)
	{
		while(IFS0bits.T1IF==0); //enquanto este bit for 0 fica nessa instrução (1 ms)
		IFS0bits.T1IF=0;
		tempo--;
	}
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB)
	// como entradas/saidas digitais
	TRISB = 0; // a PORTB como saída
	PR1 = 2000;
	//O registrador de periodo PR1 é igual a 2000 pelos seguintes motivos:
	// 1) MIPS = (Fosc * PLLx)/4 = (8 MHz * 1)/4 = 2 MIPS
	// Tcy = 1/MIPS = 1 / 2 = 0,5 useg
	// 2) Para dar 1 mseg temos que multiplicar Tcy vezes 2000.
	T1CON = 0x8000; //ativamos o timer1 e o Prescaler fica em 1.
	Delay_ms(500);
	
	/*....*/

}