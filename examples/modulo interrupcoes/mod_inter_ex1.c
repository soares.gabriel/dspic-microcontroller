/* 1.Qual é o código para habilitar a interrupção externa 1 e o timer2 do dsPIC30F? */

void main ()
{
	IFS0 = 0; //aqui se encontra o bit de flag de status da interrupção do Timer 2.
	IFS1 = 0; //aqui se encontra o bit de flag de status da interrupção Externa 1.
	IEC0bits.T2IE = 1; //Habilita a interrupção do timer 2.
	IEC1bits.INT1IE = 1; //Habilita a interrupção externa 1.
}